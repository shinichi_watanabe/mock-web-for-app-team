
function logEvent(name, params) {
    // window.alert("window: " + window);
    if (!name) {
        return;
    }

    if (window.AnalyticsWebInterface) {
        // Call Android interface
        window.AnalyticsWebInterface.logEvent(name, JSON.stringify(params));
        window.alert("Done Android.");
    } else if (window.webkit
        && window.webkit.messageHandlers
        && window.webkit.messageHandlers.firebase) {
        // Call iOS interface
        var message = {
            command: 'logEvent',
            name: name,
            parameters: params
        };
        window.webkit.messageHandlers.firebase.postMessage(message);
        window.alert("Done iOS.");
    } else {
        // No Android or iOS interface found
        window.location = "firebase://logEvent?key1=" + name;
        // window.alert("No native APIs found.");
    }
}

function setUserProperty(name, value) {
    if (!name || !value) {
        return;
    }

    if (window.AnalyticsWebInterface) {
        // Call Android interface
        window.AnalyticsWebInterface.setUserProperty(name, value);
        window.alert("Done Android.");
    } else if (window.webkit
        && window.webkit.messageHandlers
        && window.webkit.messageHandlers.firebase) {
        // Call iOS interface (WKWebView)
        var message = {
            command: 'setUserProperty',
            name: name,
            value: value
        };
        window.webkit.messageHandlers.firebase.postMessage(message);
        window.alert("Done iOS.");
    } else {
        // No Android or iOS interface found
        window.location = "firebase://setUserProperty?name=" + name + "&value=" + value;
        // window.alert("No native APIs found.");
    }
}

function logEventWithUserProperty(eventName, params, userKey, userValue) {
    // if (!eventName || !params || !userKey || !userValue) {
    //     // どれか一つでも無いのはありえないので、 or で判定
    //     return;
    // }

    // firebase_user_key と firebase_user_value というキーを、ユーザ情報設定用の予約語にする。それ以外はイベントのパラメータ
    var url = "firebase://logEventWithUserProperty/EVENT_NAME?key1=value1&key2=value2&firebase_user_key=USER_KEY&firebase_user_value=USER_VALUE";

    if (window.AnalyticsWebInterface) {
        // Android は従来の方法で良い（多分 user 設定が先）
        // window.AnalyticsWebInterface.setUserProperty(url);
        // window.AnalyticsWebInterface.logEvent(eventName, url);
        setUserProperty(userKey, userValue);
        logEvent(eventName, params);
    } else {
        // iOS (UIWebView - 今回検討しているやり方)
        window.location = url;
        // No Android or iOS interface found
        console.log("No native APIs found.");
    }

}


var $ = function(id) {
    var ret = document.getElementById(id);
    if (ret) {
        return ret;
    }
    ret = document.getElementsByName(id);
    if (ret) {
        return ret;
    }
    return null;
};
