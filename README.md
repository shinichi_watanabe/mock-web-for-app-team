## Hot to start server

1. Clone this repository

2. Move to project root.
    * `cd path/to/mock-web-for-app-team`

3. Start the server
    * `sbt run`

4. Open browser and go to:
    * `http:(your-machine).local:9000/test`  or
    * `http://localhost:9000/test`
